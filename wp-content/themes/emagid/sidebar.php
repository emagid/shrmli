<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="sidebar_home">
            <div class="wrapper">
                <div class="group">
                <h3>Attend Our Meeting</h3>
                <p>Come join the discussion with your peers on...</p>
                <div class="join_us">
                    <img style="max-width:87px;"src="<?php echo get_template_directory_uri(); ?>/assets/img/connect_bubble.png">
                </div>
                <a href="/members/" class="button"><p>Join Now</p></a>
                </div>
                <div class="group">
                    <h3>Chapter Sponsors</h3>
                    <?php
	  			$args = array(
	    		'post_type' => 'chapter_sponsors'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                    <a href="<?php the_field('website_link'); ?>" target="_blank">
                    <img src="<?php the_field('logo'); ?>">
                        </a>
        <?php
			}
				}
			else {
			echo 'No Sponsors Found';
			}
		?>   
                </div>
                <div class="group">
                    <h3>SHRM-LI Tweets</h3>
                    <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
                </div>
            </div>
        </div>
