<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/site.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/media.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="site_header">
        <div class="container">
            <div class="header_top">
                    
                <div>
                    <div class="site_logo">
                        <a href="/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="SHRMLI Logo">
                        </a>
                    </div>
                    <div class="site_social">
                        <ul>
                            <li>
                                <a href="https://www.linkedin.com/company/shrm-long-island" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin.png">
                            </a>
                                </li>
                            <li>
                            <a href="https://www.pinterest.com/shrmlipin/" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pinterest.png">
                            </a>
                                </li>
                            <li>
                            <a href="https://www.facebook.com/SHRMLongIsland?v=wall" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png">
                            </a>
                            </li>
                            <li>
                            <a href="https://twitter.com/shrmli" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png">
                            </a>
                            </li>
                                                        <li>
                            <a href="https://plus.google.com/108843189098709793484" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/google-plus.png">
                            </a>
                            </li>
                                                        <li>
                            <a href="https://www.youtube.com/user/SHRMLongIsland" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png">
                            </a>
                            </li>
                                                        <li>
                            <a href="https://www.instagram.com/shrmlongisland/" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png">
                            </a>
                            </li>
                        </ul>
                    </div>
                <div class="header_contact">
                    <a href="/register/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_man.png">
                    </a>
                    <a href="/contact/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_tel.png">
                    </a>
                </div>
                </div>
            </div>
        </div>

            <nav  class="site_nav">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    ) );
                ?>
            </nav>
        
	</header>

