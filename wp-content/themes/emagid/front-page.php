<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<div class="container_home">
        <div class="hero_home" style="background-image:url(<?php the_field('banner'); ?>)">
            <div class="overlay">
                <div class="hero_cta">
                    <h1>Human Resources<br><span>Professionals</span></h1>
                    <a href="/register" class="button"><p>Sign Up</p></a>
                </div>
            </div>
        </div>
        
        <div class="content_home">
            <div class="wrapper" id="company_summary">
                <h2><?php the_field('title'); ?></h2>
                <p><?php the_field('welcome_letter'); ?></p>
                <a href="<?php the_field('welcome_letter_link'); ?>" class="button"><p>Read More</p></a>
            </div>
                    <div class="attr_text">
            <p>
                Christel Colón, SHRM-SCP, SPHR <br>President, SHRM-Long Island
            </p>
        </div>
            
<!--
            <div class="meeting_schedule">
                <div class="wrapper">
                    <div class="date">
                        <p>Wednesday<br>
                            <strong>December</strong><br>
                            <span>20</span>
                        </p>
                    </div>
                    <div class="schedule">
                        <h3>Attend our next meeting</h3>
                        <p><span>SHRM-LI Monthly Meeting</span></p>
                        <p>Wednesday, March 28, 2018 <br>
                        8:00 AM -  10:30 AM <br>
                         <br>
                        </p>
                    </div>
                    <div class="more_info">
                        <p><span>For further info, contact:</span></p>
                        <p>SHRM-LI Chapter Executive Director<br>
                        Linda B. Selden-Paduano</p>
                        
                        <div class="contact_icons">
                            <a href="tel:1-631-262-8807">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_tel_inv.png">
                            </a>
                            <a href="mailto:executivedirector@shrmli.org" target="_top">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_mail_inv.png">
                            </a>
                        </div>
                    </div>

                </div>
            </div>
-->
            
            
            <div class="home_news">
                <div class="wrapper">
                <h2>SHRM-LI News</h2>
                    <ul>
                        
        <?php
	  			$args = array(
	    		'post_type' => 'home_news'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
 
                        <h3><?php the_field('title'); ?></h3>
                        <li>
                            <p><?php the_field('intro'); ?></p>
                            <a href="<?php the_field('link'); ?>" class="button"><p>Read More</p></a>
                        </li>
                        
        <?php
			}
				}
			else {
			echo 'No News Found';
			}
		?>   
                        
                    </ul>
                </div>
            </div>
        </div>

        
<?php
get_sidebar();?>

	</div><!-- #primary -->

<?php
get_footer();
