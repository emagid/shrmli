<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<div class="container_home">
        <div class="hero_home inner_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg)">
            <div class="hero_cta">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        
        <div class="content_home">
            <div class="wrapper" id="company_summary">
                <h2><?php the_title(); ?></h2>
                <p><?php the_field('content'); ?></p>
                
            </div>

            
            <div class="meeting_schedule">
                <div class="wrapper">
                    <div class="date">
                        <p>Wednesday<br>
                            <strong>December</strong><br>
                            <span>20</span>
                        </p>
                    </div>
                    <div class="schedule">
                        <h3>Attend our next meeting</h3>
                        <p><span>SHRM-LI Monthly Meeting</span></p>
                        <p>Wednesday, March 28, 2018 <br>
                        8:00 AM -  10:30 AM <br>
                         <br>
                        </p>
                    </div>
                    <div class="more_info">
                        <p><span>For further info, contact:</span></p>
                        <p>SHRM-LI Chapter Executive Director<br>
                        Linda B. Selden-Paduano</p>
                        
                        <div class="contact_icons">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_tel_inv.png">
                            </a>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_mail_inv.png">
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>

        
<?php
get_sidebar();?>

	</div><!-- #primary -->

<?php
get_footer();
