<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<footer class="site_footer">
        <div class="notifications">
            <h2><?php the_field('announcement'); ?> <span class="status"><a href="<?php the_field('announcement_links'); ?>">View Details</a></span></h2>
        </div>
        <div class="footer_links">
            <div class="wrapper">
                <div class="column">
                    <div class="group">
                        <h2>Sponsors</h2>
                        <ul>
                            <li>
                                <p><a href="#" target="_blank">Genser Dubow Genser & Cona LLP</a></p>
                            </li>
                            <li>
                                <p>
                                <a href="http://www.jacksonlewis.com/" target="_blank">Jackson Lewis all we do is work</a></p>
                            </li>
                            <li>
                                <p>
                                <a href="https://www.ameripriseadvisors.com/team/kuttin-wealth-management/" target="_blank">Kuttin Wealth Management</a></p>
                            </li>
                            <li>
                                <p>
                                <a href="http://seniorservicesofnorthamerica.com/retirees" target="_blank">Senior Services of North America</a>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="group">
                        <h2>Contact Us</h2>
                        <ul>
                            <li><p><strong>Phone:</strong> 631-262-8801 (Executive Director)</p></li>
                            <li><p><strong>Mailing Address:</strong><br> SHRM-LI Chapter, Inc.<br>449 Pulaski Road<br>Greenlawn, NY 11740</p></li>
                        </ul>
                        <h6>Copyright © 2018, SHRM Long Island</h6>
                    </div>
                </div>
                <div class="column">
                    <div class="group">
                        <h2>Resources</h2>
                        <ul>
                            <li>
                                <p><a href="https://www.td.org/" target="_blank">American Society Of Training & Development (ASTD)</a></p>
                            </li>
                            <li>
                                <p>
                                <a href="/community/dale-carnegie-training-long-island/">Dale Carnegie Training of Long Island</a></p>
                            </li>
                            <li>
                                <p>
                                <a href="https://www.hrci.org/" target="_blank">HR Certification Institute </a></p>
                            </li>
                            <li>
                                <p>
                                <a href="http://icf-li.com/" target="_blank">ICF-LI Chapter</a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="http://www.longislandassociation.org/" target="_blank">Long Island Association (LIA)</a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="s" target="_blank">NYS SHRM</a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="https://www.rims.org/Pages/Default.aspx" target="_blank">Risk Insurance Management Service (RIMS) </a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="https://www.shrm.org/foundation/Pages/default.aspx" target="_blank">SHRM Foundation</a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="/">SHRM Website </a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <a href="https://www.worldatwork.org/waw/flsaassistant/" target="_blank">World At Work (WAW)</a>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="column logos">
                    <div class="group">
                        <ul>
                            <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/champions.jpg">
                            </li>                   

                             <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/innovation.png">
                            </li>  
                               <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/excel-2016.jpg">
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/badge.png">
                            </li>                         
                            <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fundraise.png">
                            </li>

                            
                               <li>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/foundation.jpg">
                            </li>                         
                            
                        </ul>
                    </div>
                </div>             
            </div>
        </div>

	</footer>

<?php wp_footer(); ?>

</body>
</html>
